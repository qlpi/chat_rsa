<!doctype html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Projekt OD - klient1</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.1.1.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                function send() {
                    var username = $("#username").val();
                    var message = $("#message").val();
                    $.ajax({
                        type: 'POST',
                        url: 'send.php',
                        data: {
                            username: username,
                            message: message
                        },
                        success:function(){
                            $("#message").val("");
                        }
                    });
                }

                $("#message").keyup(function(e){
                    if(e.keyCode == 13) {
                        send();
                    }
                });

                $("#send").click(function(e){
                    send();
                });

                setInterval(function(){
                    var content = $("#content");
                    var content_rsa = $("#content_rsa");

                    content.load('download.php');
                    content.scrollTop(content[0].scrollHeight);

                    content_rsa.load('download_rsa.php');
                    content_rsa.scrollTop(content_rsa[0].scrollHeight);
                }, 1400);
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="content">Content :</label>
                        <textarea style="resize: none;" name="content" class="form-control" rows="10" id="content" readonly></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="content_rsa">Content RSA :</label>
                        <textarea style="resize: none;" name="content_rsa" class="form-control" rows="10" id="content_rsa" readonly></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="username">Username:</label>
                <input name="username" type="text" class="form-control" id="username">
            </div>
            <div class="form-group">
                <label for="message">Message:</label>
                <input name="message" type="text" class="form-control" id="message">
            </div>
            <div class="form-group">
                <input class="btn btn-primary btn-block" type="submit" value="Send" id="send">
            </div>
        </div>
    </body>

</html>


