<?php
require_once('rsa.php');

$filename = 'messages.json';
$messages = [];

if(file_exists($filename)) {
    $messages = json_decode(file_get_contents($filename), true);
    $filepri = 'rsa_pri.json';
    if(file_exists($filepri)) {
        $pri = json_decode(file_get_contents($filepri), true);
        foreach ($messages as $row) {
            echo $row['username'] . ':' . encode($pri, $row['message']) . "\n";
        }
    } else {
        echo 'Brak klucza prywatnego! Wyślij wiadomość aby wygenerować klucz.'. "\n";
    }
}