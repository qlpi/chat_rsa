<?php
$filename = 'messages.json';
$messages = [];

if(file_exists($filename)) {
    $messages = json_decode(file_get_contents($filename), true);
    foreach ($messages as $row) {
        echo $row['username'] . ':' . $row['message'] . "\n";
    }
}