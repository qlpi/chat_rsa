<?php
function phi($p, $q) {
    return ($p - 1) * ($q - 1);
}

function n($p, $q) {
    return $p * $q;
}

function nwd($a, $b) {
    while($b != 0) {
        $pom = $b;
        $b = $a % $b;
        $a = $pom;
    }
    return $a;
}

function e($phi) {
    $wynik = [];
    for($i = 2; $i <= $phi; $i++) {
        if (nwd($phi, $i) == 1) { $wynik[] = $i; }
    }
    return min($wynik);
}

function d($e, $phi) {
    $i = 1;
    while(true) {
        if((($e * $i) % $phi) == 1) { return $i; }
        $i++;
    }
}

function pub($p, $q){
    $phi = phi($p, $q);
    $n = n($p, $q);
    $e = e($phi);
    return ['e' => $e, 'n' => $n];
}

function pri($p, $q){
    $phi = phi($p, $q);
    $e = e($phi);
    $d = d($e, $phi);
    $n = n($p, $q);
    return ['d' => $d, 'n' => $n];
}

function c($pub, $t) {
    if(1 > $t && $t > $pub['n']) { return 0; }
    $bcpow = bcpow($t, $pub['e']);
    return bcmod($bcpow, $pub['n']);
}

function t($pri, $c) {
    if(0 > $c && $c > $pri['n']) { return 0; }
    $bcpow = bcpow($c, $pri['d']);
    return bcmod($bcpow, $pri['n']);
}

function decode($pub, $string) {
    $message = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $ord = ord($string[$i]);
        if (($ord > 31 && $ord < 91) || ($ord > 96 && $ord < 123)) {
            $message .= c($pub, $ord) . ' ';
        }
    }
    return $message;
}

function encode($pri, $string) {
    $message = '';
    $string = explode(' ', $string);
    foreach($string as $chr) {
        $message .= chr(t($pri, $chr));
    }
    return $message;
}

function gen($p, $q) {
    $pub = pub($p, $q);
    file_put_contents('rsa_pub.json', json_encode($pub));
    $pri = pri($p, $q);
    file_put_contents('rsa_pri.json', json_encode($pri));
}

//$p = 13;
//$q = 11;
//$pub = pub($p, $q);
//$priv = pri($p, $q);
//
//$c = c($pub, 123);
//$t = t($priv, $decode);
//
//echo $c . "\n";
//echo $t . "\n";
