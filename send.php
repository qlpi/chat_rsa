<?php
require_once('rsa.php');

if(isset($_POST['username']) && isset($_POST['message'])) {
    if(!empty($_POST['username']) && !empty($_POST['message'])) {
        $filename = 'messages.json';
        if(file_exists($filename)) {
            $filepub = 'rsa_pub.json';
            if(file_exists($filepub)) {
            } else {
                gen(13, 11);
            }
            $pub = json_decode(file_get_contents($filepub), true);
            $messages = json_decode(file_get_contents($filename), true);
            $message = decode($pub, $_POST['message']);
            $messages[] = [
                'username' => $_POST['username'],
                'message' => $message
            ];
            file_put_contents($filename, json_encode($messages));
        }
    }
}